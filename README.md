# LoRa-Drone Project

### Drone Mapping for LoRa and IoT Communications 
In the last decade drones and Unmanned Aerial Vehicles (UAVs) in general have proven to be one of the great technological developments with a range of applications from recreational hobby use to commercial applications such as monitoring, payload delivery and videography. UAVs are very useful to capture high quality aerial data at affordable cost compared to alternative approaches, resulting in development of several 3D and elevation modelling applications. The IoT low-power long-range networks, that have recently become very useful in various applications, have a common problem of a challenging mapping and difficult understanding of coverage in more complex scenarios, which would allow for effective deployments at a scale. 


Gateways in LoRa and LoraWAN networks are the interface between a large area with a significant number of nodes and the cloud. There are a number of scenarios for network coverage provisioning, Data colletion from nodes that deployed in a disaster area, deploying nodes to diffrent locations, ranging from public gateway infrastructure to dedicated gateways for specific project and private networks for a particular deployment or a scale deployment of pico gateways. The common factor when rolling out a reliable network is understanding the existing coverage in a given area and determining what coverage has been achieved once gateways have been deployed.

The LoRa-Drone project is a collaborative project between the Open Drone team and Open IoT team.

The forerunner of the project is to develop a flying LoraWAN gateway. This flying gateway will be used to collect data transmitted from sensor nodes deployed over a wide area. 

These are the four main parts of this project


##### 1. Development of Lightweight LoraWAN Gateway

##### 2. Developing a multirotor drone that capable of flying with Gateway(Payload)

##### 3. Developement of communication system for communicating with gateway(drone) from ground station

##### 4. Integrating LoraWAN Gateway and Communication system to drone



## 1. *Development of Lightweight LoraWAN Gateway*



## 2. *Developing a multirotor drone that capable of flying with Gateway(Payload)*



![Alien 680](Images/Alien680.jpg)


After developing the lightweight Lora Gateway, we analyzed the physical, electrical and RF parameters of the gateway, and designed and 3D Printed a casing for easy mounting to the drone.
physical and electrical parameters of the gateway are


Perticular          |  Value  
--------------------|----------
Weight              |   180g   
Diamenision         |   120L 60W 50H*(mm)   
Working Voltage     |   4.5 -6.0V   
Current consumption |   1.2A(max)
Power Consumption   |   5Watt(peak)


After learning the gateway parameters it working, We decided to develop a multirotor(Quadcopter) drone capable of powering the gateway and flying with it. Another major challenge we faced was to build the drone under the micro category (2000 g weight), which is due to the drone control policy in India.

### Design Characteristics
Particular | Calculation | Values |
-----------|-------------|--------|
**Total Lift Capacity** | 1650g of Thrust per motor * 4 motors = 6600g. Let's take 50% as the maximum lift capacity. [click here for simple thrust calculator ](https://www.omnicalculator.com/other/drone-motor)  | **3300g**
**Power to Weight Ratio** |If Total weight of drone shoule be well inside 2Kg and the total trust the drone can deliver is about 6600g. Hence power to weight ratio =>| **3:1**
**Average Curent Draw** | Total weight of drone  = 2000g i.e according to [motor spec sheet](https://gitlab.com/icfoss/drone/lora-drone/-/blob/master/Images/tarot_test.PNG) 50% throttle is enogh to lift the drone with 4 motors, on this configuration each motor will consume a maximum of 4A(3.88A), that accounts for a total of 16A. Lets say all the other components will consume a maximum of another 3A. | **19A**  
**Estimated Flight Time** | flight times = (Battery Capacityin Ah x (max Battery Discharge/100) /Average Amp Draw) x 60 i.e with the 7500mah battery it will be like : (80% of 7.5A / 19A) x 60 | **19 Mins**
![Motor parameter test](Images/tarot_test.PNG)



### Hardware List
Particulars| spec   |
-----------|--------|
Flight controller| [Pixhawk cube](https://docs.px4.io/en/flight_controller/pixhawk-2.html), with Intel edison in carrier board for future expansion.
GPS| [Here+ RTK GPS](http://ardupilot.org/copter/docs/common-here-plus-gps.html) for 8-10cm GPS accuracy
Remote Controller | [FrSky Taranis X9D Plus](https://www.frsky-rc.com/product/taranis-x9d-plus-2/)16 channel (up to 32 channels) fully programmable controller with audio playback support.
RC reciever | [FrSky x8R](https://www.frsky-rc.com/product/x8r/) 16channel reciever with SBUS for connection with Pixhawk cube.
ESC | [Hobbywing XRotor 40A ESC](https://www.hobbywingdirect.com/products/xrotor-40a-esc?variant=955949541) with ***no BEC***, **Transient current: 60A and Continuous current: 40A,2-6S Lipo**
Motors | [Tarot 4006 620KV](https://www.banggood.in/Tarot-4006620KV-Multi-axis-Brushless-Motor-TL68P02-p-914875.html?rmmds=search&cur_warehouse=CN), which produces 1650gms (thrust) at 19.6 Amps with 14.8v battery using a 1355 CF props
Propellers | [1355 Carbon Fiber Propellers](https://www.banggood.in/Tarot-680-PRO-TL2829-T-Series-1355-Carbon-Fiber-CW-CCW-Propellers-p-934662.html?akmClientCountry=IN&rmmds=cart_middle_products&cur_warehouse=CN)
Telemetry Module | [RFD 900+](http://store.rfdesign.com.au/rfd-900p-modem/)
Battery | 4 cell LiPo Battery (currently we use 35C Batteries with a capacity of atleast 5000mah )

## Quadcopter Wiring and Configuration
![pinout of Pixhawk Cube Flight Controller](Images/Cube.jpg)
**FlightController pinout diagram**

#### ESC-Motor-Flight Controller
Each of the 4 motors are controller by the ESC (electronic speed controllers)
which are powered directly from the power distribution board ***( here we use
100A Multirotor ESC Power Distribution Battery Board)***. 
Connect the power (+), ground (-), and signal (s) wires for each ESC to the autopilot’s main output pins by motor number.
![ESC Connection](Images/Pixhwak_outputs.jpg)



![ESC PDB](Images/Multirotor-ESC-Power-Distribution-Battery-Board.png)

The motors are connected to the flight controller in a different manner. The below
attached Motor order diagram will give a better understanding .
The diagrams show motor order for each frame type. The numbers indicate which output pin from the autopilot shoould be connected to each motor/propeller. The propeller direction is shown in green (clockwise, CW) or blue (counter-clockwise, CCW)

![Motor order Diagram](Images/motororder-quad-v-2d.png)
![Detailed Connections](Images/cube_wiring_overview.jpg)
More details in Motor order and wiring can be found
[here](http://ardupilot.org/copter/docs/connect-escs-and-motors.html).

#### RC Reciever-Flight controller
The RC reciever FrSky x8R has 8 channels with direct pwm outputs and SBUS out
for recieving all the 16 Channels through a single signal line. So the SBUS out
is connected directly to the  RCIN pin near to the MAIN OUT pins. The RCIN pin
will also power the RC Reciever.

#### Power Module-Flight Controller
The power for the flight controller is supplied by the pixhawk [power brick](http://www.proficnc.com/all-products/80-power-module.html). The power
module also measures current. **It can support upto 8S with a continous current
of upto 30 Amps**. There are two output cables from the pwoer brick. One cable
from the power module connects to POWER1 port of the Pixhawk Cube and the other
cable read and black wire is connect to the power distribution board.

#### GPS Module- Flight controller
The GPS module used is HERE+ module based on Ublox M8P with onboard safety switch.
The module is connected to the Flight Controller through the GPS1 port.

### Telemetry module
The telemetry module used here is [RFD900+](http://store.rfdesign.com.au/rfd-900p-modem/). The telemetryis
connected to the Flight controller through the Telem1 port in the pixhawk Cube.

### FrSky Telemetry


![frsky](Images/frsky_requiredhardware_flightdeck.jpg)


![Yappu](Images/x9d-taranis.png)


FrSky telemetry allows you to display ArduPilot information such as flight modes, battery level, and error messages, as well as information from additional FrSky sensors on the FrSky Taranis and other FrSky compatible RC transmitters.
For enabling this feature connect the Frsky telemetry interface module to cube flight controller and frsky radio reiever.


![Frsky](Images/41L7dSG-yIL._AC_SY200_.jpg) 

**Configuration**

Configuring SERIAL#_BAUD for FrSky telemetry has no effect - it is always a hardcoded value in ArduPilot: 9,600bps for D telemetry and 57,600bps for SmartPort telemetry.

![frskytele](Images/MP_SERIAL2_FrSky10.png)

### TF Mini Lidar Configuration

![Tf mini](Images/Alien_Lidar.jpg)


 TF 02 Lidar rangefinder device is used for measuring distance near to the ground for precision landings and altitude control, water depth, or object distance as proximity sensors for avoiding objects.
 Beanwake TF has a range upto 22m.

To configure the TF02 lidar to the Pixhawk cube, we need to change the default connector on the TF02 lidar (not compatible with the cube), after changing the connector, connect the lidar directly to any serial port on the flight controller. [Details](https://ardupilot.org/copter/docs/common-benewake-tf02-lidar.html)

![tf02](Images/benewake-tf02-pixhawk.png)


### PX4Flow Sensor Configuration

![px4](Images/Alien_PX4.png)

The PX4FLOW (Optical Flow) Sensor is a specialized high resolution downward pointing camera module and a 3-axis gyro that uses the
ground texture and visible features to determine aircraft ground velocity. Although the sensor may be supplied with a built-in Maxbotix LZ-EZ4 sonar to measure height

**Configuration with QGC**

In order to use QgroundControl, PX4Flow and ArduPilot, you will need to complete setup and focussing with the firmware loaded by QGroundControl, and then update the firmware to be compatible with ArduPilot.

1. Select the Vehicle Setup page, and click the Firmware tab.
2. Connect the PX4Flow sensor to your computer using a micro USB cable.
3.  Check the “Standard Version (stable)” is selected in the ride hand pane. Click “OK”.  QGroundControl will flash a firmware that can be used to focus the lens.
4. Unplug and replug the sensor. Two extra tabs should appear: “PX4Flow” and “Parameters”.
5. Click “PX4Flow”, remove the lens cap and point the camera at a high contrast object at least 3m away. Remove the small screw that stops the lens from turning and adjust the focus until the image appears clearly. This will focus the device to infinity. Refit the screw.
6. Download and unzip the [PX4Flow-KLT firmware (source code here)](https://download.ardupilot.org/downloads/wiki/advanced_user_tools/px4flow-klt-06Dec2014.zip)
7. Unplug the sensor, click on the “Firmware” tab and replug the sensor.
8. On the right hand side, click on the firmware version dropdown, and select “Custom firmware file”. Click “OK”. Then select the firmware downloaded above. QGroundControl should now flash a firmware compatible with ArduPilot. QGroundControl will now think that the sensor is a Pixhawk. Dont worry. Unplug it, and connect it to your autopilot
